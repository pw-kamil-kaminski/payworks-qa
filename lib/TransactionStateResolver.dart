import 'package:terminal/states_description.dart';

class TransactionStateResolver {
  TerminalState resolve(String amount, bool isContact) {
    amount = amount.split(RegExp("[\.,]"))[0];

    // Standard Scenarios
    if (amount == "10") {
      return AskForSignatureState(amount);
    } else if (amount == "40") {
      return EnterPinState(amount, "", fallbackToSignature: false);
    } else if (amount == "100") {
      return DeclinedState(amount);
    }

    TransactionEndResult endResult = getTransactionEndResult(amount);
    switch (endResult) {
      case TransactionEndResult.APPROVE:
        if (_isNoCvm(amount)) {
          return ProcessingState(amount);
        } else if (_isSignature(amount)) {
          return AskForSignatureState(amount);
        } else if ((_isOfflinePin(amount) ||
            _isOfflinePinAndSignature(amount))) {
          if (isContact) {
            return EnterPinState(amount, "",
                fallbackToSignature: _isOfflinePinAndSignature(amount),
                endResult: _getTransactionEndResultBasedOnPin(amount));
          } else {
            return ProcessingState(amount);
          }
        } else if (_isOnlinePin(amount)) {
          return EnterPinState(amount, "",
              fallbackToSignature: false, endResult: endResult);
        } else {
          return ProcessingState(amount);
        }
        break;
      case TransactionEndResult.DECLINE:
        if (_isNoCvm(amount)) {
          return DeclinedState(amount);
        } else if (_isSignature(amount)) {
          return AskForSignatureState(amount, endResult: endResult);
        } else if ((_isOfflinePin(amount) ||
            _isOfflinePinAndSignature(amount))) {
          if (isContact) {
            return EnterPinState(amount, "",
                fallbackToSignature: _isOfflinePinAndSignature(amount),
                endResult: endResult);
          } else {
            return DeclinedState(amount);
          }
        } else if (_isOnlinePin(amount)) {
          return EnterPinState(amount, "",
              fallbackToSignature: false, endResult: endResult);
        } else {
          return DeclinedState(amount);
        }
        break;
      case TransactionEndResult.CARD_BLOCKED:
        if (_isNoCvm(amount)) {
          return CardBlockedState(amount);
        } else if (_isSignature(amount)) {
          return AskForSignatureState(amount, endResult: endResult);
        } else if ((_isOfflinePin(amount) ||
            _isOfflinePinAndSignature(amount))) {
          if (isContact) {
            return EnterPinState(amount, "",
                fallbackToSignature: _isOfflinePinAndSignature(amount),
                endResult: endResult);
          } else {
            return CardBlockedState(amount);
          }
        } else if (_isOnlinePin(amount)) {
          return EnterPinState(amount, "",
              fallbackToSignature: false, endResult: endResult);
        } else {
          return CardBlockedState(amount);
        }
        break;
    }

    return ProcessingState(amount);
  }

  // =========== Card Behavior ===========

  TransactionEndResult getTransactionEndResult(String amount) {
    if (_isAuthorized(amount)) {
      return TransactionEndResult.APPROVE;
    } else if (_isImmediatelyDecline(amount) ||
        _isDeclineAfterAuthorization(amount)) {
      return TransactionEndResult.DECLINE;
    } else if (_isCardBlocked(amount)) {
      return TransactionEndResult.CARD_BLOCKED;
    }

    return TransactionEndResult.APPROVE;
  }

  bool _isAuthorized(String amount) {
    return (amount.length == 3 && amount.substring(0, 1) == "0") ||
        (amount.length == 2);
  }

  bool _isImmediatelyDecline(String amount) {
    return amount.length == 3 && amount.substring(0, 1) == "1";
  }

  bool _isDeclineAfterAuthorization(String amount) {
    return amount.length == 3 && amount.substring(0, 1) == "2";
  }

  bool _isCardBlocked(String amount) {
    return amount.length == 3 && amount.substring(0, 1) == "3";
  }

  // =========== CVM ===========

  bool _isNoCvm(String amount) {
    return (amount.length == 3 && amount.substring(1, 2) == "0") ||
        (amount.length == 2 && amount.substring(0, 1) == "0");
  }

  bool _isSignature(String amount) {
    return (amount.length == 3 && amount.substring(1, 2) == "1") ||
        (amount.length == 2 && amount.substring(0, 1) == "1");
  }

  bool _isOfflinePin(String amount) {
    return (amount.length == 3 && amount.substring(1, 2) == "2") ||
        (amount.length == 2 && amount.substring(0, 1) == "2");
  }

  bool _isOfflinePinAndSignature(String amount) {
    return (amount.length == 3 && amount.substring(1, 2) == "3") ||
        (amount.length == 2 && amount.substring(0, 1) == "3");
  }

  bool _isOnlinePin(String amount) {
    return amount.length == 3 && amount.substring(1, 2) == "4" ||
        (amount.length == 2 && amount.substring(0, 1) == "4");
  }

  // =========== PIN Verification Result ===========

  bool _isExpectedPinOk(String amount) {
    if (amount.length == 3 && amount.substring(2, 3) == "0") {
      return true;
    } else if (amount.length == 3 && amount.substring(2, 3) == "1") {
      return false;
    } else if (amount.length == 2 && amount.substring(1, 2) == "1") {
      return false;
    } else {
      return true;
    }
  }

  TransactionEndResult _getTransactionEndResultBasedOnPin(String amount) {
    if (_isExpectedPinOk(amount)) {
      return TransactionEndResult.APPROVE;
    }
    return TransactionEndResult.DECLINE;
  }
}

enum TransactionEndResult { APPROVE, DECLINE, CARD_BLOCKED }
