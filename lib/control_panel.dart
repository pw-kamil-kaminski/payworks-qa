import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class ControlPanel extends StatefulWidget {
  ControlPanel({
    Key key,
    @required this.onPressed,
  }) : super(key: key);
  final Function(ControlPanelEvent event, String payload) onPressed;

  @override
  _ControlPanelState createState() => _ControlPanelState();
}

class _ControlPanelState extends State<ControlPanel> {
  final amountTextController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 380,
      width: 300,
      margin: EdgeInsets.symmetric(horizontal: 128, vertical: 0),
      decoration: BoxDecoration(
          border: Border.all(
        width: 1,
        color: Colors.black,
      )),
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text(
              "POS System",
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 8, top: 8, right: 8),
            child: TextField(
              controller: amountTextController,
              decoration: InputDecoration(border: OutlineInputBorder()),
              keyboardType: TextInputType.numberWithOptions(decimal: true),
              inputFormatters: [
                BlacklistingTextInputFormatter(new RegExp('[a-zA-Z]'))
              ],
            ),
          ),
          ButtonTheme(
            minWidth: 150,
            child: FlatButton(
              child: Text("Start transaction"),
              onPressed: () {
                widget.onPressed.call(
                    ControlPanelEvent.START_TX, amountTextController.text);
              },
              color: Colors.green,
              textColor: Colors.white,
            ),
          ),
          ButtonTheme(
            minWidth: 150,
            child: FlatButton(
              child: Text("Abort transaction"),
              onPressed: () {
                widget.onPressed.call(ControlPanelEvent.ABORT_TX, null);
              },
              color: Colors.red,
              textColor: Colors.white,
            ),
          ),
          ButtonTheme(
            minWidth: 150,
            child: FlatButton(
              child: Text("Accept signature"),
              onPressed: () {
                widget.onPressed.call(ControlPanelEvent.SIGNATURE_OK, null);
              },
              color: Colors.blue,
              textColor: Colors.white,
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 8.0),
            child: Divider(
              color: Colors.black,
              height: 1,
              thickness: 1,
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text(
              "Client actions",
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
            ),
          ),
          ButtonTheme(
            minWidth: 150,
            child: FlatButton(
                child: Text("Insert CHIP card"),
                onPressed: () {
                  widget.onPressed.call(ControlPanelEvent.CARD_ICC, null);
                },
                color: Colors.blue,
                textColor: Colors.white),
          ),
          ButtonTheme(
            minWidth: 150,
            child: FlatButton(
              child: Text("Use NFC card"),
              onPressed: () {
                widget.onPressed.call(ControlPanelEvent.CARD_NFC, null);
              },
              color: Colors.blue,
              textColor: Colors.white,
            ),
          ),
//          FlatButton(
//            child: Text("Remove card"),
//            onPressed: () {
//              onPressed.call(ControlPanelEvent.REMOVE_CARD, null);
//            },
//            color: Colors.blue,
//            textColor: Colors.white,
//          ),
        ],
      ),
    );
  }
}

enum ControlPanelEvent {
  START_TX,
  ABORT_TX,
  CARD_ICC,
  CARD_NFC,
  REMOVE_CARD,
  SIGNATURE_OK
}
