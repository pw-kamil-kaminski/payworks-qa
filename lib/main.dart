import 'package:flutter/material.dart';
import 'package:terminal/states_description.dart';
import 'package:terminal/terminal.dart';

import 'control_panel.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Terminal Emulator - Payworks',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  TerminalState state = IdleState();

  @override
  Widget build(BuildContext context) {
    handleAsyncStateChanges();

    return Scaffold(
      body: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Terminal(
              firstLine: state.getFirstLine(),
              secondLine: state.getSecondLine(),
              onPinPadKeyPressed: handlePinPadKeyClick),
          ControlPanel(
            onPressed: handleControlPanelEvent,
          )
        ],
      ),
    );
  }

  handlePinPadKeyClick(pinPadKey) {
    TerminalState newState = state.consumePinPadKey(pinPadKey);
    if (newState != state) {
      state.onCancel();
      setState(() {
        state = newState;
      });
    }
  }

  handleControlPanelEvent(controlPanelEvent, payload) {
    TerminalState newState =
        state.consumeControlPanelEvent(controlPanelEvent, payload);
    if (newState != state) {
      state.onCancel();
      setState(() {
        state = newState;
      });
    }
  }

  Future<void> handleAsyncStateChanges() async {
    state.subscribeToFutureState().then((futureState) => {
          if (futureState != null)
            setState(() {
              state = futureState;
            })
        });
  }
}
