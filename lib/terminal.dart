import 'package:flutter/material.dart';

class Terminal extends StatelessWidget {
  const Terminal({
    Key key,
    @required this.firstLine,
    @required this.secondLine,
    @required this.onPinPadKeyPressed,
  }) : super(key: key);

  final String firstLine;
  final String secondLine;
  final Function(PinPadKey key) onPinPadKeyPressed;

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Container(
          decoration: BoxDecoration(
              color: Colors.black,
              borderRadius: BorderRadius.all(Radius.circular(10)),
              border: Border.all(width: 2, color: Colors.black)),
          child: Column(
            children: <Widget>[
              _TextDisplay(
                firstRow: firstLine,
                secondRow: secondLine,
              ),
              Row(
                children: [
                  _PinPadButton(
                      pinPadKey: PinPadKey.KEY_1,
                      onPressed: onPinPadKeyPressed),
                  _PinPadButton(
                      pinPadKey: PinPadKey.KEY_2,
                      onPressed: onPinPadKeyPressed),
                  _PinPadButton(
                      pinPadKey: PinPadKey.KEY_3,
                      onPressed: onPinPadKeyPressed),
                ],
              ),
              Row(
                children: [
                  _PinPadButton(
                      pinPadKey: PinPadKey.KEY_4,
                      onPressed: onPinPadKeyPressed),
                  _PinPadButton(
                      pinPadKey: PinPadKey.KEY_5,
                      onPressed: onPinPadKeyPressed),
                  _PinPadButton(
                      pinPadKey: PinPadKey.KEY_6,
                      onPressed: onPinPadKeyPressed),
                ],
              ),
              Row(
                children: [
                  _PinPadButton(
                      pinPadKey: PinPadKey.KEY_7,
                      onPressed: onPinPadKeyPressed),
                  _PinPadButton(
                      pinPadKey: PinPadKey.KEY_8,
                      onPressed: onPinPadKeyPressed),
                  _PinPadButton(
                      pinPadKey: PinPadKey.KEY_9,
                      onPressed: onPinPadKeyPressed),
                ],
              ),
              Row(
                children: [
                  _PinPadButton(
                      pinPadKey: PinPadKey.KEY_F,
                      onPressed: onPinPadKeyPressed),
                  _PinPadButton(
                      pinPadKey: PinPadKey.KEY_0,
                      onPressed: onPinPadKeyPressed),
                  _PinPadButton(
                      pinPadKey: PinPadKey.KEY_DOT,
                      onPressed: onPinPadKeyPressed),
                ],
              ),
              Row(
                children: [
                  _PinPadButton(
                      pinPadKey: PinPadKey.KEY_RED,
                      buttonColor: Colors.red,
                      onPressed: onPinPadKeyPressed),
                  _PinPadButton(
                      pinPadKey: PinPadKey.KEY_YELLOW,
                      buttonColor: Colors.yellow,
                      onPressed: onPinPadKeyPressed),
                  _PinPadButton(
                      pinPadKey: PinPadKey.KEY_GREEN,
                      buttonColor: Colors.green,
                      onPressed: onPinPadKeyPressed),
                ],
              ),
            ],
          ),
        ),
      ],
    );
  }
}

class _TextDisplay extends StatelessWidget {
  const _TextDisplay({Key key, this.firstRow = "", this.secondRow = ""})
      : super(key: key);
  final String firstRow;
  final String secondRow;

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Container(
          decoration: BoxDecoration(
              color: Colors.white,
              border: Border.all(width: 1, color: Colors.black)),
          height: 96,
          width: 256,
          margin: EdgeInsets.all(16),
          alignment: AlignmentDirectional.center,
          child: Text(
            '$firstRow\n$secondRow',
            textAlign: TextAlign.center,
            style: TextStyle(
              fontSize: 20,
              height: 1.5,
            ),
          ),
        )
      ],
    );
  }
}

class _PinPadButton extends StatelessWidget {
  const _PinPadButton(
      {Key key,
      this.pinPadKey,
      this.buttonColor = Colors.grey,
      @required this.onPressed})
      : super(key: key);
  final PinPadKey pinPadKey;
  final Color buttonColor;
  final Function(PinPadKey pinPadKey) onPressed;

  @override
  Widget build(BuildContext context) {
    return Container(
        padding: EdgeInsets.all(4.0),
        child: FlatButton(
            onPressed: () {
              onPressed.call(pinPadKey);
            },
            color: buttonColor,
            child: Text(
              getKeyText(),
              style: TextStyle(fontSize: 18),
            )));
  }

  String getKeyText() {
    switch (pinPadKey) {
      case PinPadKey.KEY_1:
      case PinPadKey.KEY_2:
      case PinPadKey.KEY_3:
      case PinPadKey.KEY_4:
      case PinPadKey.KEY_5:
      case PinPadKey.KEY_6:
      case PinPadKey.KEY_7:
      case PinPadKey.KEY_8:
      case PinPadKey.KEY_9:
      case PinPadKey.KEY_0:
      case PinPadKey.KEY_F:
        return pinPadKey.toString().substring(14);
      case PinPadKey.KEY_DOT:
        return '.';
      case PinPadKey.KEY_RED:
      case PinPadKey.KEY_YELLOW:
      case PinPadKey.KEY_GREEN:
        return "";
    }
    return pinPadKey.toString();
  }
}

enum PinPadKey {
  KEY_1,
  KEY_2,
  KEY_3,
  KEY_4,
  KEY_5,
  KEY_6,
  KEY_7,
  KEY_8,
  KEY_9,
  KEY_0,
  KEY_DOT,
  KEY_F,
  KEY_RED,
  KEY_YELLOW,
  KEY_GREEN
}
