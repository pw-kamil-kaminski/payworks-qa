import 'dart:async';
import 'package:async/async.dart';
import 'package:terminal/TransactionStateResolver.dart';

import 'package:terminal/terminal.dart';

import 'control_panel.dart';

/// Base class for terminal state
class TerminalState {
  TerminalState(this.amount, [this.pin]);

  final String amount;
  final String pin;
  CancelableOperation<TerminalState> _futureState;

  TerminalState consumePinPadKey(PinPadKey pinPadKey) => this;

  TerminalState consumeControlPanelEvent(
      ControlPanelEvent event, String payload) {
    return this;
  }

  String getFirstLine() => "";

  String getSecondLine() => "";

  /// simulates asynchronous state change, for tx processing
  CancelableOperation<TerminalState> subscribeToFutureState() {
    _futureState = _getFutureState();
    return _futureState;
  }

  CancelableOperation<TerminalState> _getFutureState() {
    return CancelableOperation.fromFuture(null);
  }

  void onCancel() {
    if (_futureState != null) _futureState.cancel();
  }
}

class IdleState extends TerminalState {
  IdleState() : super(null, null);

  @override
  String getFirstLine() => "Welcome";

  @override
  TerminalState consumeControlPanelEvent(
      ControlPanelEvent event, String payload) {
    switch (event) {
      case ControlPanelEvent.START_TX:
        return PresentCardState(payload);
    }
    return this;
  }
}

class PresentCardState extends TerminalState {
  PresentCardState(String amount) : super(amount);

  @override
  String getFirstLine() => "Present card";

  @override
  String getSecondLine() => "$amount\$";

  @override
  TerminalState consumeControlPanelEvent(
      ControlPanelEvent event, String payload) {
    switch (event) {
      case ControlPanelEvent.ABORT_TX:
        return AbortedState();
      case ControlPanelEvent.CARD_ICC:
        return IccCardInsertedState(amount);
      case ControlPanelEvent.CARD_NFC:
        return NfcCardInsertedState(amount);
    }
    return this;
  }

  @override
  TerminalState consumePinPadKey(PinPadKey pinPadKey) {
    switch (pinPadKey) {
      case PinPadKey.KEY_RED:
        return AbortedState();
    }
    return super.consumePinPadKey(pinPadKey);
  }
}

class IccCardInsertedState extends TerminalState {
  IccCardInsertedState(String amount) : super(amount);

  @override
  CancelableOperation<TerminalState> _getFutureState() {
    TransactionStateResolver stateResolver = TransactionStateResolver();
    return stateResolver.resolve(amount, true).immediately();
  }
}

class NfcCardInsertedState extends TerminalState {
  NfcCardInsertedState(String amount) : super(amount);

  @override
  CancelableOperation<TerminalState> _getFutureState() {
    TransactionStateResolver stateResolver = TransactionStateResolver();
    return stateResolver.resolve(amount, false).immediately();
  }
}

class EnterPinState extends TerminalState {
  EnterPinState(String amount, String pin,
      {this.fallbackToSignature, this.endResult})
      : super(amount, pin);

  final bool fallbackToSignature;
  final TransactionEndResult endResult;

  @override
  String getFirstLine() => "Enter PIN";

  @override
  String getSecondLine() => pin;

  @override
  TerminalState consumeControlPanelEvent(
      ControlPanelEvent event, String payload) {
    switch (event) {
      case ControlPanelEvent.ABORT_TX:
        return AbortedState();
    }
    return this;
  }

  @override
  TerminalState consumePinPadKey(PinPadKey pinPadKey) {
    switch (pinPadKey) {
      case PinPadKey.KEY_1:
      case PinPadKey.KEY_2:
      case PinPadKey.KEY_3:
      case PinPadKey.KEY_4:
      case PinPadKey.KEY_5:
      case PinPadKey.KEY_6:
      case PinPadKey.KEY_7:
      case PinPadKey.KEY_8:
      case PinPadKey.KEY_9:
      case PinPadKey.KEY_0:
        if (pin.length < 4) {
          return EnterPinState(amount, pin + "*",
              fallbackToSignature: fallbackToSignature, endResult: endResult);
        }
        return this;
      case PinPadKey.KEY_DOT:
      case PinPadKey.KEY_F:
        break;
      case PinPadKey.KEY_RED:
        return AbortedState();
      case PinPadKey.KEY_YELLOW:
        return EnterPinState(amount, pin.substring(0, pin.length - 1),
            fallbackToSignature: fallbackToSignature, endResult: endResult);
      case PinPadKey.KEY_GREEN:
        switch (endResult) {
          case TransactionEndResult.APPROVE:
            if (fallbackToSignature) {
              return AskForSignatureState(amount);
            } else {
              return ProcessingState(amount);
            }
            break;
          case TransactionEndResult.DECLINE:
            return DeclinedState(amount);
          case TransactionEndResult.CARD_BLOCKED:
            return CardBlockedState(amount);
        }
    }
    return super.consumePinPadKey(pinPadKey);
  }
}

class ProcessingState extends TerminalState {
  ProcessingState(String amount) : super(amount);

  @override
  CancelableOperation<TerminalState> _getFutureState() {
    return ApprovedState(amount).delayed(2);
  }

  @override
  String getFirstLine() => "Processing transaction...";

  @override
  TerminalState consumeControlPanelEvent(
      ControlPanelEvent event, String payload) {
    switch (event) {
      case ControlPanelEvent.ABORT_TX:
        return AbortedState();
    }
    return this;
  }
}

class ApprovedState extends TerminalState {
  ApprovedState(String amount) : super(amount);

  @override
  String getFirstLine() {
    return "Approved";
  }

  @override
  CancelableOperation<TerminalState> _getFutureState() {
    return IdleState().delayed(2);
  }
}

class AbortedState extends TerminalState {
  AbortedState() : super(null);

  @override
  String getFirstLine() {
    return "Aborted";
  }

  @override
  CancelableOperation<TerminalState> _getFutureState() {
    return IdleState().delayed(2);
  }
}

class AskForSignatureState extends TerminalState {
  AskForSignatureState(String amount,
      {this.endResult = TransactionEndResult.APPROVE})
      : super(amount);
  final TransactionEndResult endResult;

  @override
  String getFirstLine() {
    return "Signature verification";
  }

  @override
  TerminalState consumeControlPanelEvent(
      ControlPanelEvent event, String payload) {
    switch (event) {
      case ControlPanelEvent.SIGNATURE_OK:
        switch (endResult) {
          case TransactionEndResult.APPROVE:
            return ProcessingState(amount);
          case TransactionEndResult.DECLINE:
            return DeclinedState(amount);
          case TransactionEndResult.CARD_BLOCKED:
            return CardBlockedState(amount);
        }
        break;
      case ControlPanelEvent.ABORT_TX:
        return AbortedState();
    }
    return super.consumeControlPanelEvent(event, payload);
  }

  @override
  TerminalState consumePinPadKey(PinPadKey pinPadKey) {
    switch (pinPadKey) {
      case PinPadKey.KEY_RED:
        return AbortedState();
    }
    return super.consumePinPadKey(pinPadKey);
  }
}

class DeclinedState extends TerminalState {
  DeclinedState(String amount) : super(amount);

  @override
  String getFirstLine() {
    return "Declined";
  }

  @override
  CancelableOperation<TerminalState> _getFutureState() {
    return IdleState().delayed(2);
  }
}

class CardBlockedState extends TerminalState {
  CardBlockedState(String amount) : super(amount);

  @override
  String getFirstLine() {
    return "Card is blocked";
  }

  @override
  CancelableOperation<TerminalState> _getFutureState() {
    return IdleState().delayed(2);
  }
}

extension TerminalStateExtensions on TerminalState {
  CancelableOperation<TerminalState> immediately() =>
      CancelableOperation.fromFuture(Future.value(this));

  CancelableOperation<TerminalState> delayed(seconds) =>
      CancelableOperation.fromFuture(
          Future.delayed(Duration(seconds: seconds), () => this));
}
